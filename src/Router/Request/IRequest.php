<?php


namespace App\Router\Request;


interface IRequest
{
    public function getBody();
}