<?php
require 'vendor/autoload.php';

use App\Controllers\NewsController;
use App\Request\Request;
use App\Router\Router;

$request = new Request();
//$router = new Router($request);
$router = new Router();


//$router->get('/', function() {
//  return <<<HTML
//  <h1>Hello world</h1>
//HTML;
//});
//
//
//$router->get('/profile', function($request) {
//  return <<<HTML
//  <h1>Profile</h1>
//HTML;
//});
//
//$router->post('/data', function($request) {
//
//  return var_dump($request->getBody());
//});
//
//$router->get('/news', function(\App\Request\Request $request) {
//    return (new \App\Controllers\NewsController())->getAll($request);
//});
//

// Add the first route
//Router::add('/user/([0-9]*)/edit', function($id) {
//    echo 'Edit user with id '.$id.'<br>';
//}, 'get');

//$router->post('/news/', function ($request) {
//    (new NewsController())->create($request);
////    return json_encode($request->getBody());
//});
//
//$router->get('/news/:id', function ($id) {
//    (new NewsController())->get($id);
////    return json_encode($request->getBody());
//});
//
//$router->post('/news/{id}', function ($request) {
//    (new NewsController())->update($request);
////    return json_encode($request->getBody());
//});


// 2 //


Router::map('get', '/news', function () {
    (new NewsController())->getAll(new Request());
});
//
//
Router::map('get', '/news/([0-9]*)', function ($id) {
    (new NewsController())->get($id);
});

Router::map('post', '/news/([0-9]*)', function ($id) {
    (new NewsController())->update(Router::request(), $id);
});

Router::map('post', '/news', function () {
    (new NewsController())->create(Router::request());
});

// Run the router
Router::match();
//$router->resolve();