<?php


namespace App\Tests\Database;


use App\Database\Connection\Connection;
use App\News\Models\News;
use App\User\Models\User;
use PHPUnit\Framework\TestCase;

/**
 * For testing in development environment
 *
 * Class ConnectionTest
 * @package App\Tests\Database
 */
class AbstractModelTest extends TestCase
{

    public function testPdoCreatedAndTablesAvailable()
    {
        $news = new News(new Connection());

        $result = $news->getExistingTables();
        $this->assertTrue(in_array('news', $result));
        $this->assertTrue(in_array('permission', $result));
        $this->assertTrue(in_array('user', $result));
        $this->assertTrue(in_array('permission_role', $result));
    }

    public function test_select()
    {
        $user = new User(new Connection());
        $result = $user->select(['id', 'name'], ['id' => 1]);

        $expected = ['id' => '1', 'name' => 'test_admin'];
        $this->assertEquals($expected, $result[0]);
    }

    public function test_insert()
    {
        $user = new User(new Connection());
        $result = $user->insert(['id' => '3', 'name' => 'testing', 'password' => 'stwewrwe', 'role_id' => '1']);

        $this->assertEquals(3, $result);
    }

    public function test_update()
    {
        $user = new User(new Connection());
        $result = $user->update(['name' => 'sdfsdfds'], ['id' => '3']);

        $this->assertTrue($result);
    }

    public function test_update_two_where()
    {
        $user = new User(new Connection());
        $result = $user->update(['name' => '55'], ['id' => '3', 'name' => 'sdfsdfds']);
        $this->assertTrue($result);
    }

    public function test_delete()
    {
        $user = new User(new Connection());
        $result = $user->delete(['id' => '3']);
        $this->assertTrue($result);
    }

    public function test_select_not_conditions()
    {
        $news = new News(new Connection());
        $result = $news->select(['title', 'text']);

        $expected = ['title' => 'My First news', 'text' => 'This is test news'];
        $this->assertEquals($expected, $result[0]);
    }

}