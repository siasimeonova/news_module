<?php


namespace App\Tests\Database\Connection;


use App\Database\Connection\QueryBuilder;
use PHPUnit\Framework\TestCase;

/**
 * For testing in development environment
 *
 * Class ConnectionTest
 * @package App\Tests\Database
 */
class QueryBuilderTest extends TestCase
{
    /**
     * @test
     */
    public function test_get_select_query()
    {
        $table = 'user';
        $columns = ['id', 'name'];
        $bindings = ['id' => 1];

        $query = (new QueryBuilder())->selectQuery($table, $columns, $bindings);
        $expectedResult = "SELECT id, name FROM `user` WHERE id=?;";
        $this->assertEquals($expectedResult, $query);
    }

    /**
     * @test
     */
    public function test_get_insert_query()
    {
        $table = 'user';
        $columns = ['id', 'name'];

        $query = (new QueryBuilder())->insertQuery($table, $columns);
        $expectedResult = "INSERT INTO `user` (`id`, `name`) VALUES (?, ?);";
        $this->assertEquals($expectedResult, $query);
    }

    /**
     * @test
     */
    public function test_get_update_query()
    {
        $table = 'user';
        $columns = ['name' => 'test'];
        $bindings = ['id' => 1];

        $query = (new QueryBuilder())->updateQuery($table, $columns, $bindings);
        $expectedResult = "UPDATE `user` SET name=? WHERE id=?;";
        $this->assertEquals($expectedResult, $query);
    }

    /**
     * @test
     */
    public function test_get_delete_query()
    {
        $table = 'user';
        $bindings = ['id' => 1];

        $query = (new QueryBuilder())->deleteQuery($table, $bindings);
        $expectedResult = "DELETE FROM `user` WHERE id=?;";
        $this->assertEquals($expectedResult, $query);
    }


    /**
     * @test
     */
    public function test_get_select_query_no_where()
    {
        $table = 'user';
        $columns = ['id', 'name'];

        $query = (new QueryBuilder())->selectQuery($table, $columns);
        $expectedResult = "SELECT id, name FROM `user`;";
        $this->assertEquals($expectedResult, $query);
    }
}