<?php


namespace App\Tests\User\Models;

use App\Database\Connection\Connection;
use App\User\Models\User;
use PHPUnit\Framework\TestCase;

/**
 * For testing in development environment
 *
 * Class UserTest
 * @package App\Tests\User\Models
 */
class UserTest extends TestCase
{

    public function test_get_by_token()
    {
        $result = (new User(new Connection()))->getByToken('sgthfdhwf3405w43586perm8y845');

        $this->assertArrayHasKey('id', $result);
        $this->assertEquals(1, $result['id']);
    }

    public function test_get_by_token_error()
    {
        $result = (new User(new Connection()))->getByToken('fgh');
        $this->assertEquals([], $result);
    }

    public function test_get_user_permission()
    {
        $result = (new User(new Connection()))->getUserPermission(1);
        $this->assertEquals(['News#create', 'News#delete', 'News#update'], $result);
    }

    public function test_get_user_permission_no_permission()
    {
        $result = (new User(new Connection()))->getUserPermission(2);
        $this->assertEquals([], $result);
    }

    public function test_get_user_permission_no_permission_error_id()
    {
        $result = (new User(new Connection()))->getUserPermission(0);
        $this->assertEquals([], $result);
    }
}