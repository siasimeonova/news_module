<?php


namespace App\Database\Connection;


use App\Database\Connection\Credentials\ConnectionCredentials;
use PDO;

/**
 * Class Connection
 * @package App\Database\Connection
 *
 * Create PDO object and manage database communication
 */
class Connection implements ExecuteQueryInterface
{
    /**
     * @var string Default database type
     */
    private static $databaseType = 'mysql';
    /**
     * @var PDO
     */
    private $pdo;

    /**
     * @var QueryBuilder Service object for sql query building.
     */
    private $queryBuilder;

    public function __construct(array $credentials = [], QueryBuilder $queryBuilder = null)
    {
        $this->setPdo($credentials);
        $this->setQueryBuilder($queryBuilder);
    }

    private function setPdo($credentials)
    {
        if (!empty($credentials)) {
            $dsn = $credentials['driver'] . ':dbname=' . $credentials['news_module'] . ';host=' . $credentials['host'];
            $pdo = new PDO($dsn, $credentials['username'], $credentials['password'], $credentials['options']);
        } else {
            $credentials = ConnectionCredentials::getDatabaseCredentials(static::$databaseType);
            $dsn = $credentials['driver'] . ':dbname=' . $credentials['database'] . ';host=' . $credentials['host'];
            $pdo = new PDO($dsn, $credentials['username'], $credentials['password']);
        }

        $this->pdo = $pdo;
    }

    private function setQueryBuilder(QueryBuilder $queryBuilder = null)
    {
        if (isset($queryBuilder)) {
            $this->queryBuilder = $queryBuilder;
        } else {
            $this->queryBuilder = new QueryBuilder();
        }
    }

    public function getPdo()
    {
        return $this->pdo;
    }

    public function getExistingTables()
    {
        $query = $this->queryBuilder->getExistingTablesQuery();
        return $this->pdo->query($query)->fetchAll(PDO::FETCH_COLUMN);
    }

    public function select($table, array $columns, array $bindings = [])
    {
        $query = $this->queryBuilder->selectQuery($table, $columns, $bindings);

        if (empty($bindings)) {
            return $this->pdo->query($query)->fetchAll(PDO::FETCH_ASSOC);
        }

        $stm = $this->bindParams($query, $bindings);
        $stm->execute();
        return $stm->fetchAll(PDO::FETCH_ASSOC);
    }

    private function bindParams($query, $bindings)
    {
        $stm = $this->pdo->prepare($query);
        $counter = 1;
        foreach ($bindings as $column => $value) {
            $stm->bindValue($counter++, $value, PDO::PARAM_STR);
        }

        return $stm;
    }

    public function insert($table, array $columns)
    {
        $query = $this->queryBuilder->insertQuery($table, array_keys($columns));
        $result = $this->executeQuery($query, $columns);

        if ($result) {
            return $this->pdo->lastInsertId();
        }

        return 0;
    }


    /**
     * Prepare and execute query, return operation result - mixed value.
     *
     * @param $query
     * @param $bindings
     * @return boolean
     */
    public function executeQuery(string $query, array $bindings)
    {
        $stm = $this->bindParams($query, $bindings);
        $result = $stm->execute();

        if (!$result) {
            // TODO:: Logger log error
//            var_dump($stm->debugDumpParams());
            return false;
        }

        return ($stm->rowCount() > 0);
    }

    public function update($table, array $columns, array $bindings)
    {
        $query = $this->queryBuilder->updateQuery($table, $columns, $bindings);

        $allBindings = $columns;
        foreach ($bindings as $binding) {
            $allBindings[] = $binding;
        }

        return $this->executeQuery($query, $allBindings);
    }

    public function delete($table, array $bindings)
    {
        $query = $this->queryBuilder->deleteQuery($table, $bindings);
        return $this->executeQuery($query, $bindings);
    }

    /**
     * Prepare and execute query, return operation result.
     *
     * @param $query
     * @param $bindings
     * @return array
     */
    public function executeSelectQuery(string $query, array $bindings)
    {
        $stm = $this->bindParams($query, $bindings);
        $result = $stm->execute();

        if (!$result) {
            // TODO:: Logger log error
//            return $stm->debugDumpParams();
            return [];
        }

        return $stm->fetchAll(PDO::FETCH_ASSOC);
    }
}