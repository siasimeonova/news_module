<?php


namespace App\Database\Connection\Credentials;


/**
 * Class ConnectionCredentials
 * @package App\Database\Connection\Credentials
 *
 * Holds default database credentials
 */
final class ConnectionCredentials
{

    private static $configs = [
        'mysql' => [
            'driver' => 'mysql',
            'host' => 'localhost',
            'database' => 'news_module',
            'username' => 'root',
            'password' => '1234567890'
        ]
    ];

    private function __construct()
    {
    }

    /**
     * Return default database credentials
     *
     * @param $databaseType
     * @return mixed
     */
    public static function getDatabaseCredentials($databaseType)
    {
        if(!key_exists($databaseType, static::$configs))
        {
            throw new \InvalidArgumentException('Invalid database type!');
        }

        return static::$configs[$databaseType];
    }

}