<?php


namespace App\Router;

use App\Router\Request\Request;

class Router
{

    private static $matchExpresion = '([0-9A-Za-z]*)';
    private static $placeholderFindExpresion = '/(\[)*:(\$)*\w+(])*/i';
    private $routes = [];
    private $pathNotFound = null;
    private $methodNotAllowed = null;

    /**
     * Add url to action mapping
     *
     * @param string $method Request method (post, get, delete)
     * @param string $url Path to be mapped
     * @param mixed $action The callable or method that will handle the request
     */
    public function map(string $method, string $url, $action)
    {
        if (is_string($action)) {
            $action = $this->splitClassAndMethodName($action);
        } elseif (is_array($action)) {
            $action[0] = $this->createClassName($action[0]);
        }

        $urlWithMatchFunctions = preg_replace(
            static::$placeholderFindExpresion,
            static::$matchExpresion,
            $url);

        array_push($this->routes, Array(
            'url' => $urlWithMatchFunctions,
            'action' => $action,
            'method' => $method
        ));
    }

    /**
     * Split string action to array with class name and method name.
     *
     * @param string $url
     * @return array|null
     */
    private function splitClassAndMethodName(string $url)
    {
        $elements = explode('#', $url);

        $className = $this->createClassName($elements[0]);
        $method = $elements[1];
        if (class_exists($className) && method_exists(new $className(), $method)) {
            return [$className, $method];
        } else {
            return $this->pathNotFound;
        }
    }

    /**
     * @param $name
     * @return string
     */
    private function createClassName($name)
    {
        return 'App\\' . $name . '\Controllers\\' . $name . 'Controller';
    }

    /**
     * Set callable to be executed if path not found
     *
     * @param mixed $action
     */
    public function pathNotFound($action)
    {
        $this->pathNotFound = $action;
    }

    /**
     * Set callable to be executed if request method not matched as allowed
     *
     * @param mixed $action
     */
    public function methodNotAllowed($action)
    {
        $this->methodNotAllowed = $action;
    }

    /**
     * Process request, if request url is mapped to action
     * call mapped function to process the request data. Call methodNotAllowed or pathNotFound if set.
     */
    public function match()
    {
        $parsedUrl = parse_url($_SERVER['REQUEST_URI']);

        if (isset($parsedUrl['path']) && $parsedUrl['path'] != '/') {
            $path = rtrim($parsedUrl['path'], '/');
        } else {
            $path = '/';
        }

        // Get request method
        $method = $_SERVER['REQUEST_METHOD'];

        $pathFound = false;
        foreach ($this->routes as $route) {
            // Add 'start' end 'end'
            $route['url'] = '^' . $route['url']. '$';

            // Check path match
            if (preg_match('#' . $route['url'] . '#' . 'i', $path, $matches)) {
                $pathFound = true;
                // Match method
                if (strtolower($method) == strtolower($route['method'])) {
                    array_shift($matches); // Remove first element with the whole string

                    if ($method == 'POST' && is_array($route['action'])) {
                        // Action has been set as array or string
                        array_unshift($matches, new Request());
                    }

                    call_user_func_array($route['action'], $matches);
                    return;
                }
            }
        }

        if ($pathFound) {
            if ($this->methodNotAllowed) {
                call_user_func_array($this->methodNotAllowed, Array($path, $method));
            }
        } else {
            if ($this->pathNotFound) {
                call_user_func_array($this->pathNotFound, Array($path));
            }
        }
    }
}