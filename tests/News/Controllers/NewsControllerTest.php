<?php


namespace App\Tests\News\Models;

use App\Database\Connection\Connection;
use App\News\Controllers\NewsController;
use App\Router\Request\Request;
use PHPUnit\Framework\TestCase;

/**
 * For testing in development environment
 *
 * Class UserTest
 * @package App\Tests\Database\Models
 */
class NewsControllerTest extends TestCase
{

    public function test_get_all()
    {
        $mockRequest = $this->createMock(Request::class);
        $mockRequest->method('getBody')
            ->willReturn([
                'title' => 'My First news',
                'date' => '2020-01-30 21:47:23',
                'text' => 'This is test news'
            ]);

        $controller = new NewsController();
        $controller->getAll($mockRequest);
        $expected = 'string(92) "[{"id":"1","title":"My First news","date":"2020-01-30 21:47:23","text":"This is test news"}]"';
        $this->assertEquals($expected, trim(ob_get_contents()));
    }

    public function test_get()
    {
        $controller = new NewsController();
        $controller->get(1);
        $expected = 'string(90) "{"id":"1","title":"My First news","date":"2020-01-30 21:47:23","text":"This is test news"}"';
        $this->assertEquals($expected, trim(ob_get_contents()));
    }

    public function test_get_wrong_id()
    {
        $controller = new NewsController();
        $controller->get(1967896787);
        $expected = 'string(38) "["News does not exist or not loaded."]"';
        $this->assertEquals($expected, trim(ob_get_contents()));
    }


    public function test_create_no_token()
    {
        $mockRequest = $this->createMock(Request::class);
        $mockRequest->method('getBody')
            ->willReturn([
                'title' => 'My SECOND news',
                'date' => '2020-01-30 21:47:23',
                'text' => 'This is test news'
            ]);

        $controller = new NewsController();
        $controller->create($mockRequest);

        $expected = 'string(30) "{"error":"Token is required!"}"';
        $this->assertEquals($expected, trim(ob_get_contents()));
    }


    public function test_create_wrong_token()
    {
        $mockRequest = $this->createMock(Request::class);
        $mockRequest->method('getBody')
            ->willReturn([
                'title' => 'My SECOND news',
                'date' => '2020-01-30 21:47:23',
                'text' => 'This is test news',
                'token' => '1'
            ]);


        $controller = new NewsController();
        $controller->create($mockRequest);

        $expected = 'string(35) "{"error":"Not authenticated user!"}"';
        $this->assertEquals($expected, trim(ob_get_contents()));
    }

    public function test_create_no_permissions()
    {
        $mockRequest = $this->createMock(Request::class);
        $mockRequest->method('getBody')
            ->willReturn([
                'title' => 'My SECOND news',
                'date' => '2020-01-30 21:47:23',
                'text' => 'This is test news',
                'token' => 'sgthfdhwf3405w43586perm8y111'
            ]);


        $controller = new NewsController();
        $controller->create($mockRequest);

        $expected = 'string(34) "{"error":"Operation not allowed!"}"';
        $this->assertEquals($expected, trim(ob_get_contents()));
    }

    public function test_create_success()
    {
        $mockRequest = $this->createMock(Request::class);
        $mockRequest->method('getBody')
            ->willReturn([
                'title' => 'My SECOND news',
                'date' => '2020-01-30 21:47:23',
                'text' => 'This is test news',
                'token' => 'sgthfdhwf3405w43586perm8y845'
            ]);


        $controller = new NewsController();
        $controller->create($mockRequest);

        $expected = 'string(95) "[{"id":"102","title":"My SECOND news","date":"2020-01-30 21:47:23","text":"This is test news"}]"';
        $result = trim(ob_get_contents());
        $this->assertTrue(strpos($result, '"title":"My SECOND news"') != false);
    }


    public function test_update()
    {
        $result = (new Connection())->select('news', ['max(id) as id']);
        $id = (reset($result)['id']);

        $mockRequest = $this->createMock(Request::class);
        $mockRequest->method('getBody')
            ->willReturn([
                'title' => 'My Last news',
                'date' => '2020-01-30 21:47:23',
                'text' => 'This is test news',
                'token' => 'sgthfdhwf3405w43586perm8y845'
            ]);

        $controller = new NewsController();
        $controller->update($mockRequest, $id);

        $expected = 'string(11) "["SUCCESS"]"';
        $this->assertEquals($expected, trim(ob_get_contents()));
    }

    public function test_update_no_differences_in_update_data()
    {
        $result = (new Connection())->select('news', ['max(id) as id']);
        $id = (reset($result)['id']);

        $mockRequest = $this->createMock(Request::class);
        $mockRequest->method('getBody')
            ->willReturn([
                'title' => 'My Last news',
                'date' => '2020-01-30 21:47:23',
                'text' => 'This is test news',
                'token' => 'sgthfdhwf3405w43586perm8y845'
            ]);

        $controller = new NewsController();
        $controller->update($mockRequest, $id);

        $expected = 'string(25) "["ERROR OR DATA IS SAME"]"';
        $this->assertEquals($expected, trim(ob_get_contents()));
    }


    public function test_update_id_not_found()
    {
        $result = (new Connection())->select('news', ['max(id) as id']);
        $id = (reset($result)['id']);

        $mockRequest = $this->createMock(Request::class);
        $mockRequest->method('getBody')
            ->willReturn([
                'title' => 'My Last news',
                'date' => '2020-01-30 21:47:23',
                'text' => 'This is test news',
                'token' => 'sgthfdhwf3405w43586perm8y845'
            ]);

        $controller = new NewsController();
        $controller->update($mockRequest, $id + 1);

        $expected = 'string(25) "["ERROR OR DATA IS SAME"]"';
        $this->assertEquals($expected, trim(ob_get_contents()));
    }


    public function test_delete()
    {
        $result = (new Connection())->select('news', ['max(id) as id']);
        $id = (reset($result)['id']);

        $controller = new NewsController();
        $controller->delete($id + 1, 'sgthfdhwf3405w43586perm8y845');

        $expected = 'string(9) "["ERROR"]"';
        $this->assertEquals($expected, trim(ob_get_contents()));
    }

    public function test_delete_succeess()
    {
        $result = (new Connection())->select('news', ['max(id) as id']);
        $id = (reset($result)['id']);

        $controller = new NewsController();
        $controller->delete($id, 'sgthfdhwf3405w43586perm8y845');

        $expected = 'string(11) "["SUCCESS"]"';
        $this->assertEquals($expected, trim(ob_get_contents()));
    }

}