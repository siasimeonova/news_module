<?php


namespace App\Database\Connection;


interface ExecuteQueryInterface
{
    /**
     * Execute query to database
     *
     * @param string $query
     * @param array $bindings
     * @return mixed
     */
        public function executeQuery(string $query, array $bindings);
        public function executeSelectQuery(string $query, array $bindings);
}