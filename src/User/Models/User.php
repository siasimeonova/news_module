<?php


namespace App\User\Models;

use App\Database\AbstractModel;
use InvalidArgumentException;

class User extends AbstractModel
{

    protected static $table = 'user';

    protected static $properties = [
        'name',
        'password',
    ];


    /**
     * Add user to database
     *
     * @param array $properties
     * @return boolean Result of the insert
     */
    public function add(array $properties)
    {
        if (!isset($properties['name']) || !isset($properties['name'])) {
            throw new InvalidArgumentException('Username and password are mandatory!');
        }

        $userProperties = ['name' => $properties['name'], 'password' => $properties['password']];
        return $this->insert($userProperties);
    }

    public function getUserPermission(string $roleId)
    {
        $permissionsQ = "SELECT `permission`.`name` 
                    FROM `permission` 
                    JOIN `permission_role` 
                    ON `permission`.`id` = `permission_role`.`permission_id`
                    WHERE `permission_role`.`role_id` = ?
        ";

        $results = $this->executeSelectQuery($permissionsQ, [$roleId]);
        $permissions = [];
        foreach ($results as $result) {
            $permissions[] = $result['name'];
        }

        return $permissions;
    }

    /**
     * Return user data if the argument value has been found in the database or null otherwise.
     *
     * @param string $token
     * @return array An empty array or array with user data
     */
    public function getByToken(string $token)
    {
        $query = 'SELECT `user`.`id` , `user`.`role_id` 
                    FROM `user` 
                    JOIN `api_token` 
                    ON `user`.`id` = `api_token`.`user_id`
                    WHERE `api_token`.`token` = ?';

        $records = $this->executeSelectQuery($query, [$token]);
        if (count($records) !== 1) {
            return [];
        }

        return reset($records);
        // TODO Create permission validation
    }

}