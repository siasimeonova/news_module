<?php


namespace App\Database\Connection;

/**
 * Create SQL String query using given params. Can be used only for simple queries.
 *
 * Class QueryBuilder
 * @package App\Database\Connection
 */
class QueryBuilder
{

    public function getExistingTablesQuery()
    {
        return 'SHOW TABLES';
    }

    public function selectQuery($table, array $columns, array $bindings = [])
    {
        $query = 'SELECT ' . implode(', ', $columns) . ' FROM `' . $table . '`';
        if (!empty($bindings)) {
            $query .= ' WHERE ';
            foreach (array_keys($bindings) as $column) {
                $query .= $column . '=? AND ';
            }
            $query = rtrim($query, ' AND ');
        }

        return $query . ';';
    }

    public function insertQuery($table, array $columnNames)
    {
        $query = 'INSERT INTO `' . $table . "` (`" . implode("`, `", $columnNames) . "`) VALUES (";
        foreach (($columnNames) as $column) {
            $query .= '?, ';
        }
        $query = rtrim($query, ', ') . ');';

        return $query;
    }

    public function updateQuery($table, array $columns, array $bindings)
    {
        $columnNames = array_keys($columns);
        $query = 'UPDATE `' . $table . "` SET ";
        foreach (array_keys($columns) as $column) {
            $query .= $column . '=?, ';
        }

        $query = rtrim($query, ', ') . ' WHERE ';
        foreach (array_keys($bindings) as $condition) {
            $query .= $condition . '=? AND ';
        }

        $query = rtrim($query, ' AND ') . ';';
        return $query;
    }

    public function deleteQuery($table, array $bindings)
    {
        $query = 'DELETE FROM `' . $table . '` WHERE ';
        foreach (array_keys($bindings) as $column) {
            $query .= $column . '=? AND ';
        }
        $query = rtrim($query, ' AND ') . ';';

        return $query;
    }
}