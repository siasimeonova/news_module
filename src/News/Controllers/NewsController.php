<?php


namespace App\News\Controllers;


use App\Database\Connection\Connection;
use App\News\Models\News;
use App\Router\Request\Request;
use App\User\Models\User;
use Exception;

class NewsController
{

    static $className = 'News';

    public function getAll(Request $request)
    {
        $news = static::getModel()->getAll();
        if (empty($news)) {
            var_dump(json_encode(['No news']));
            return;
        }
        var_dump(json_encode($news));
    }

    protected static function getModel(Connection $conn = null)
    {
        $conn = isset($conn) ?: new Connection();
        return new News($conn);
    }

    public function get($id)
    {
        $news = static::getModel()->get($id);
        if (empty($news)) {
            var_dump(json_encode(['News does not exist or not loaded.']));
            return;
        }
        var_dump(json_encode(reset($news)));
    }

    public function create(Request $request)
    {
        $requestData = $request->getBody();
        if (!isset($requestData['token'])) {
            var_dump(json_encode(['error' => 'Token is required!']));
            return;
        }

        // Authenticate and validate permission
        $userId = 0;
        try {
            $userId = static::authenticateUser($requestData['token'], __FUNCTION__);
        } catch (Exception $ex) {
            var_dump(($ex->getMessage()));
            return;
        }

        $properties = [
            'title' => $requestData['title'],
            'created_at' => $requestData['date'],
            'text' => $requestData['text'],
            'created_by' => $userId,
        ];

        $model = static::getModel();
        $result = $model->create($properties);
        if ($result !== 0) {
            $news = $model->get($result);
            var_dump(json_encode($news));
            return;
        }

        var_dump(json_encode([]));
    }

    protected static function authenticateUser($token, $action)
    {
        $userModel = new User(new Connection());
        $user = $userModel->getByToken($token);
        if (empty($user)) {
            throw new Exception(json_encode(['error' => 'Not authenticated user!']));
        }

        $userPermissions = $userModel->getUserPermission($user['role_id']);
        $methodPermission = static::$className . '#' . $action;
        if (empty($userPermissions) || !in_array($methodPermission, $userPermissions)) {
            throw new Exception(json_encode(['error' => 'Operation not allowed!']));
        }

        return $user['id'];
    }

    public function update(Request $request, $id)
    {
        $requestData = $request->getBody();
        if (!isset($requestData['token'])) {
            var_dump(json_encode(['error' => 'Token is required!']));
            return;
        }

        // Authenticate and validate permission
        $userId = 0;
        try {
            $userId = static::authenticateUser($requestData['token'], __FUNCTION__);
        } catch (Exception $ex) {
            var_dump(($ex->getMessage()));
            return;
        }

        $properties = [
            'title' => $requestData['title'],
            'created_at' => $requestData['date'],
            'text' => $requestData['text'],
            'created_by' => $userId,
        ];

        $result = static::getModel()->update($properties, ['id' => $id]);
        $result = $result ? ['SUCCESS'] : ['ERROR OR DATA IS SAME'];
        var_dump(json_encode($result));
    }

    public function delete(int $id, string $token)
    {
        // Authenticate and validate permission
        $userId = 0;
        try {
            $userId = static::authenticateUser($token, __FUNCTION__);
        } catch (Exception $ex) {
            var_dump(($ex->getMessage()));
            return;
        }

        $result = static::getModel()->delete(['id' => $id]);
        $result = $result ? ['SUCCESS'] : ['ERROR'];
        var_dump(json_encode($result));
    }

}