<?php


namespace App\News\Models;

use App\Database\AbstractModel;

class News extends AbstractModel
{
    protected static $table = 'news';


    public function getAll()
    {
        return $this->select(['id', 'title', 'created_at as date', 'text']);
    }

    public function get(int $id)
    {
        return $this->select(['id', 'title', 'created_at as date', 'text'], ['id' => $id]);
    }

    public function create(array $properties)
    {
        $data = [
            'title' => $properties['title'],
            'created_at' => $properties['created_at'],
            'text' => $properties['text'],
            'created_by' => $properties['created_by'],
        ];

        return $this->insert($data);
    }
}