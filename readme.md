# Module for accessing news data via API end points.

The purpouse of the module is to handle CRUD operation on news objects using API end points.

## Installation

Run composer install


## Run unit tests

The unit tests are in tests folder of the project.
