<?php


namespace App\Tests\News\Models;

use App\Database\Connection\Connection;
use App\News\Models\News;
use PHPUnit\Framework\TestCase;

/**
 * For testing in development environment
 *
 * Class UserTest
 * @package App\Tests\News\Models
 */
class NewsTest extends TestCase
{

    public function test_get_all()
    {
        $result = (new News(new Connection()))->getAll();
        $expected = [
            'id' => '1',
            'title' => 'My First news',
            'date' => '2020-01-30 21:47:23',
            'text' => 'This is test news'
        ];
        $this->assertEquals($expected, $result['0']);
    }

    public function test_get()
    {
        $result = (new News(new Connection()))->get(1);
        $expected = [
            'id' => '1',
            'title' => 'My First news',
            'date' => '2020-01-30 21:47:23',
            'text' => 'This is test news'
        ];
        $this->assertEquals($expected, $result['0']);
    }

    public function test_create()
    {
        $data = [
            'title' => 'My First news',
            'created_at' => '2020-01-30 21:47:23',
            'text' => 'This is test news',
            'created_by' => 1,
        ];

        $result = (new News(new Connection()))->create($data);

        $maxRecord = (new Connection())->select('news', ['max(id) as id']);
        $id = (reset($maxRecord)['id']);

        $this->assertEquals($id, $result);
    }

    public function test_update()
    {
        $result = (new Connection())->select('news', ['max(id) as id']);
        $id = (reset($result)['id']);

        $data = [
            'title' => 'My Second news',
            'created_at' => '2020-01-30 21:47:23',
            'text' => 'This is test news',
            'created_by' => 1,
        ];

        $result = (new News(new Connection()))->update($data, ['id' => $id]);
        $this->assertTrue($result);
    }

    public function test_delete()
    {
        $result = (new Connection())->select('news', ['max(id) as id']);
        $id = (reset($result)['id']);
        $result = (new News(new Connection()))->delete(['id' => $id]);
        $this->assertTrue($result);
    }

    public function test_update_not_existing_record()
    {
        $result = (new Connection())->select('news', ['max(id) as id']);
        $id = (reset($result)['id']);

        $data = [
            'title' => 'My Second news',
            'created_at' => '2020-01-30 21:47:23',
            'text' => 'This is test news',
            'created_by' => 1,
        ];

        $result = (new News(new Connection()))->update($data, ['id' => $id + 1]);
        $this->assertFalse($result);
    }

    public function test_delete_not_existing_record()
    {
        $result = (new Connection())->select('news', ['max(id) as id']);
        $id = (reset($result)['id']);

        $result = (new News(new Connection()))->delete(['id' => $id + 1]);
        $this->assertFalse($result);
    }

    public function test_get_not_existing_record()
    {
        $result = (new Connection())->select('news', ['max(id) as id']);
        $id = (reset($result)['id']);

        $result = (new News(new Connection()))->get($id + 1);
        $this->assertEquals([], $result);
    }

}