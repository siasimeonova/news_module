<?php


namespace App\Database;


use App\Database\Connection\Connection;
use App\Database\Connection\QueryBuilder;
use PDO;

abstract class AbstractModel
{

    protected static $table;
    /**
     * @var Connection
     */
    protected $pdo;
    /**
     * @var QueryBuilder
     */
    protected $queryBuilder;

    public function __construct($connection, QueryBuilder $builder = null)
    {
        $this->pdo = $connection->getPdo();
        $builder = isset($builder) ?: new QueryBuilder();
        $this->setQueryBuilder($builder);
    }

    private function setQueryBuilder(QueryBuilder $queryBuilder = null)
    {
        if (isset($queryBuilder)) {
            $this->queryBuilder = $queryBuilder;
        } else {
            $this->queryBuilder = new QueryBuilder();
        }
    }

    public function getExistingTables()
    {
        $query = $this->queryBuilder->getExistingTablesQuery();
        return $this->pdo->query($query)->fetchAll(PDO::FETCH_COLUMN);
    }

    public function select(array $columns, array $bindings = [])
    {
        $query = $this->queryBuilder->selectQuery(static::$table, $columns, $bindings);

        if (empty($bindings)) {
            return $this->pdo->query($query)->fetchAll(PDO::FETCH_ASSOC);
        }

        $stm = $this->bindParams($query, $bindings);
        $stm->execute();
        return $stm->fetchAll(PDO::FETCH_ASSOC);
    }

    private function bindParams($query, $bindings)
    {
        $stm = $this->pdo->prepare($query);
        $counter = 1;
        foreach ($bindings as $column => $value) {
            $stm->bindValue($counter++, $value, PDO::PARAM_STR);
        }

        return $stm;
    }

    public function insert(array $columns)
    {
        $query = $this->queryBuilder->insertQuery(static::$table, array_keys($columns));
        $result = $this->executeQuery($query, $columns);

        if ($result) {
            return $this->pdo->lastInsertId();
        }

        return 0;
    }

    /**
     * Prepare and execute query, return operation result - mixed value.
     *
     * @param $query
     * @param $bindings
     * @return boolean
     */
    public function executeQuery(string $query, array $bindings)
    {
        $stm = $this->bindParams($query, $bindings);
        $result = $stm->execute();

        if (!$result) {
            // TODO:: Logger log error
//            var_dump($stm->debugDumpParams());
            return false;
        }

        return ($stm->rowCount() > 0);
    }

    public function update(array $columns, array $bindings)
    {
        $query = $this->queryBuilder->updateQuery(static::$table, $columns, $bindings);

        $allBindings = $columns;
        foreach ($bindings as $binding) {
            $allBindings[] = $binding;
        }

        return $this->executeQuery($query, $allBindings);
    }

    public function delete(array $bindings)
    {
        $query = $this->queryBuilder->deleteQuery(static::$table, $bindings);
        return $this->executeQuery($query, $bindings);
    }

    /**
     * Prepare and execute query, return operation result.
     *
     * @param $query
     * @param $bindings
     * @return array
     */
    public function executeSelectQuery(string $query, array $bindings)
    {
        $stm = $this->bindParams($query, $bindings);
        $result = $stm->execute();

        if (!$result) {
            // TODO:: Logger log error
//            return $stm->debugDumpParams();
            return [];
        }

        return $stm->fetchAll(PDO::FETCH_ASSOC);
    }
}